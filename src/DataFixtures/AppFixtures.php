<?php

namespace App\DataFixtures;

use App\Entity\TelegramCommand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;

/**
 * Class AppFixtures
 *
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $command = (new TelegramCommand())
            ->setName('check')
            ->setClassName('App\\TelegramCommand\\CheckTelegramCommand')
            ->setDescription('Return the status of api')
            ->setCallback(false)
            ->setCreatedAt(new \DateTime());

        $user = (new User())
            ->setAdmin(true)
            ->setBot(false)
            ->setName('Nicolas')
            ->setCreatedAt(new \DateTime())
            ->setEnabled(true)
            ->setChatId(602308649);

        $manager->persist($user);
        $manager->persist($command);
        $manager->flush();
    }
}
