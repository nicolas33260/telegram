<?php

namespace App\Repository;

use App\Entity\TelegramCommand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TelegramCommand|null find($id, $lockMode = null, $lockVersion = null)
 * @method TelegramCommand|null findOneBy(array $criteria, array $orderBy = null)
 * @method TelegramCommand[]    findAll()
 * @method TelegramCommand[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TelegramCommandRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TelegramCommand::class);
    }

    // /**
    //  * @return TelegramCommand[] Returns an array of TelegramCommand objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TelegramCommand
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
