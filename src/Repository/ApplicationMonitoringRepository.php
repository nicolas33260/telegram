<?php

namespace App\Repository;

use App\Entity\ApplicationMonitoring;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ApplicationMonitoring|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApplicationMonitoring|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApplicationMonitoring[]    findAll()
 * @method ApplicationMonitoring[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationMonitoringRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ApplicationMonitoring::class);
    }

    // /**
    //  * @return ApplicationMonitoring[] Returns an array of ApplicationMonitoring objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ApplicationMonitoring
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
