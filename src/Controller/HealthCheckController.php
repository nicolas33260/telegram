<?php


namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

/**
 * Class HealthCheck
 *
 * @package App\Controller
 */
class HealthCheckController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(
     *     name="healthcheck",
     *     path="/v1/healthcheck"
     * )
     *
     * @return View
     */
    public function healthCheckAction():View
    {
        return $this->view([
            'api' => 'available',
            'version' => '0.1.0'
        ]);
    }
}
