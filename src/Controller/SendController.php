<?php

namespace App\Controller;

use App\Manager\TelegramManager;
use App\Repository\UserRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;

/**
 * Class SendController
 *
 * @package App\Controller
 * @Rest\Route("/v1")
 */
class SendController extends AbstractFOSRestController
{
    /**
     * @Rest\Post(
     *     name="send_message",
     *     path="/send"
     * )
     *
     * @Rest\RequestParam(name="message", description="Message to send", nullable=false)
     *
     * @param ParamFetcher    $fetcher
     * @param TelegramManager $manager
     *
     * @return View
     */
    public function updatesAction(ParamFetcher $fetcher, TelegramManager $manager):View
    {
        $message = $fetcher->get('message');

        $manager->sendMessage(
            $this->getUser()->getChatId(), $message
        );

        return $this->view('message send');
    }
}
