<?php

namespace App\Controller;

use App\Manager\UpdateManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;

/**
 * Class TelegramController
 *
 * @package App\Controller
 */
class TelegramController extends AbstractFOSRestController
{
    /**
     * @Rest\Post(
     *     name="telegram_updates",
     *     path="/hook/updates"
     * )
     *
     * @Rest\RequestParam(name="update_id", nullable=false)
     * @Rest\RequestParam(name="message", nullable=true)
     * @Rest\RequestParam(name="callback_query", nullable=true)
     *
     * @param ParamFetcher    $fetcher
     * @param UpdateManager   $manager
     *
     * @return View
     */
    public function updatesAction(ParamFetcher $fetcher, UpdateManager $manager):View
    {
        $message = $fetcher->get('message');
        $callback = $fetcher->get('callback_query');

        if( $message ) {
            $manager->process($message);
        }

        if( $callback ) {
            $manager->processCallback($callback);
        }

        return $this->view('receive');
    }
}
