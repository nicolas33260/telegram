<?php


namespace App\Controller;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use App\Manager\TelegramManager;
use App\Repository\UserRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BitbucketController
 *
 * @package App\Controller
 */
class BitbucketController extends AbstractFOSRestController
{
    /**
     * @Rest\Post(
     *     name="bitbucket_push",
     *     path="/hook/bitbucket/push"
     * )
     * @Rest\RequestParam(name="actor", nullable=false)
     * @Rest\RequestParam(name="push", nullable=false)
     * @Rest\RequestParam(name="repository", nullable=false)
     *
     * @param ParamFetcher    $fetcher
     * @param TelegramManager $manager
     * @param UserRepository  $repository
     * @param Environment     $twig
     *
     * @return Response
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function bitbucketPushAction(ParamFetcher $fetcher, TelegramManager $manager, UserRepository $repository, Environment $twig):Response
    {
        $actor = $fetcher->get('actor');
        $push = $fetcher->get('push');
        $repo = $fetcher->get('repository');

        $message = $twig->render('Bitbucket/Push/push.message.html.twig', [
            'actor' => $actor,
            'push' => $push,
            'repo' => $repo
        ]);

        $users = $repository->findBy(['enabled' => true]);

        foreach ($users as $user){
            $manager->sendMessage($user->getChatId(), $message);
        }

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Post(
     *     name="bitbucket_merge",
     *     path="/hook/bitbucket/merge"
     * )
     * @Rest\RequestParam(name="pullrequest", nullable=false)
     * @Rest\RequestParam(name="repository", nullable=false)
     * @Rest\RequestParam(name="actor", nullable=false)
     *
     * @param ParamFetcher    $fetcher
     * @param TelegramManager $manager
     * @param UserRepository  $repository
     * @param Environment     $twig
     *
     * @return Response
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function bitbucketMergeAction(ParamFetcher $fetcher, TelegramManager $manager, UserRepository $repository, Environment $twig):Response
    {
        $actor = $fetcher->get('actor');
        $pullRequest = $fetcher->get('pullrequest');
        $repo = $fetcher->get('repository');

        $message = $twig->render('Bitbucket/PullRequest/pull.request.message.html.twig', [
            'actor' => $actor,
            'pullRequest' => $pullRequest,
            'repo' => $repo
        ]);

        $users = $repository->findBy(['enabled' => true]);

        foreach ($users as $user){
            $manager->sendMessage($user->getChatId(), $message);
        }

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
