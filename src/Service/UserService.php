<?php


namespace App\Service;


use App\Manager\TelegramManager;
use App\Manager\UserManager;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class UserService
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var TelegramManager
     */
    protected $telegramManager;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * UserService constructor.
     *
     * @param UserManager         $userManager
     * @param TelegramManager     $telegramManager
     * @param TranslatorInterface $translator
     */
    public function __construct(UserManager $userManager, TelegramManager $telegramManager, TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->telegramManager = $telegramManager;
        $this->userManager = $userManager;
    }

    /**
     * @param Request $request
     *
     * @return Response|null
     *
     * @throws Exception
     */
    public function checkUser(Request $request):?Response
    {
        $json = json_decode($request->getContent(), true);

        if( false === $json ) {
            return new Response(null, Response::HTTP_NO_CONTENT);
        }

        if( !isset($json['message']) && !isset($json['callback_query']) ) {
            return new Response('Message and Callback not found', Response::HTTP_OK);
        }

        $mainKey = isset($json['message']) ? 'message' : 'callback_query';

        $chatId = $json[$mainKey]['from']['id'];

        $user = $this->userManager
            ->fetch($chatId);

        if( !$user ) {

            $message = $json['message']['text'];
            $name = $json['message']['from']['first_name'];

            if( $message == 'sign' ) {

                $this->userManager->create($name, $chatId);
            }

            return new Response(null, Response::HTTP_NO_CONTENT);
        }

        if( !$user->getEnabled() ) {
            $this->telegramManager->sendMessage(
                $chatId,
                $this->translator->trans('sign.unauthorised')
            );

            return new Response(null, Response::HTTP_NO_CONTENT);
        }

        return null;
    }
}