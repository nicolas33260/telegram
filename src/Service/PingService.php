<?php


namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class Ping
 *
 * @package App\Service
 */
class PingService
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * PingService constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $uri
     *
     * @return string
     */
    public function pong(string $uri):string
    {
        $httpClient = HttpClient::create();

        try{
            $start = microtime(true);
            $response = $httpClient->request('GET', $uri, [
                'headers' => [
                    'Content-type' => 'text/html; charset=utf-8',
                    'Accept' => '*/*'
                ]
            ]);
            $end = microtime(true);
            $time = $end - $start;
            $statusCode = $response->getStatusCode();

            if($statusCode === 200){
                return '✅ - time : '. number_format(($time*1000), 2). ' ms';
            }

        } catch (TransportExceptionInterface $exception) {
            $this->logger->error($exception->getMessage());
        }

        return 'down';
    }
}
