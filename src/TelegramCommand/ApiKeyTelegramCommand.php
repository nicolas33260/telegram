<?php

namespace App\TelegramCommand;

use App\Entity\User;
use App\Interfaces\TelegramCommandInterface;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ApiKeyTelegramCommand implements TelegramCommandInterface
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var int
     */
    protected $chatId;

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * ApiKeyTelegramCommand constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return $this->user->getApiKey();
    }

    /**
     * @param int $chatId
     */
    public function setChatId(int $chatId): void
    {
        $this->chatId = $chatId;
        $this->user = $this->repository
            ->findOneBy(['chatId' => $chatId]);
    }

    /**
     * @param string $args
     */
    public function setArgs(string $args):void
    {
    }
}
