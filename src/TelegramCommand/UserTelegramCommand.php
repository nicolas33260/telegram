<?php

namespace App\TelegramCommand;

use App\Interfaces\TelegramCommandInterface;
use App\Manager\TelegramManager;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserTelegramCommand implements TelegramCommandInterface
{
    /**
     * @var int
     */
    protected $userId;

    /**
     * @var boolean
     */
    protected $accept;

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var EntityManagerInterface
     */
    protected $manager;

    /**
     * @var TelegramManager
     */
    protected $telegramManager;

    /**
     * UserTelegramCommand constructor.
     *
     * @param UserRepository         $repository
     * @param EntityManagerInterface $manager
     * @param TelegramManager        $telegramManager
     */
    public function __construct(UserRepository $repository, EntityManagerInterface $manager, TelegramManager $telegramManager)
    {
        $this->telegramManager = $telegramManager;
        $this->repository = $repository;
        $this->manager = $manager;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $user = $this->repository->find($this->userId);

        if($this->accept) {

            $user->setEnabled($this->accept);
            $this->manager->flush();

            $this->telegramManager->sendMessage(
                $user->getChatId(), 'Un administrateur a accepté votre demande'
            );
        }

        return $user->getName().' a été '. (  $this->accept ? 'accepté' : 'refusé' );
    }

    /**
     * @param string $args
     *
     * @return mixed|void
     */
    public function setArgs(string $args):void
    {
        list($userId, $response) = explode('=', $args);

        $this->userId = (int)$userId;
        $this->accept = $response === 'yes';
    }

    public function setChatId(int $chatId): void
    {

    }
}
