<?php

namespace App\TelegramCommand;

use App\Entity\ApplicationMonitoring;
use App\Interfaces\TelegramCommandInterface;
use App\Repository\ApplicationMonitoringRepository;
use App\Service\PingService;

class CheckTelegramCommand implements TelegramCommandInterface
{
    /**
     * @var ApplicationMonitoringRepository
     */
    protected $repository;

    /**
     * @var PingService
     */
    protected $ping;

    /**
     * CheckTelegramCommand constructor.
     *
     * @param ApplicationMonitoringRepository $repository
     * @param PingService                     $ping
     */
    public function __construct(ApplicationMonitoringRepository $repository, PingService $ping)
    {
        $this->repository = $repository;
        $this->ping = $ping;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $apps = $this->repository->findAll();

        $message = '';

        foreach ($apps as $app) {
            /** @var ApplicationMonitoring $app */
            $pong = $this->ping->pong($app->getUri());
            $message .= sprintf("<b>%s</b> are %s \n", $app->getName(), $pong);
            $message .= "---------------------------------\n";
        }

        return $message;
    }

    /**
     * @param int $chatId
     */
    public function setChatId(int $chatId): void
    {
    }

    /**
     * @param string $args
     */
    public function setArgs(string $args):void
    {
    }
}
