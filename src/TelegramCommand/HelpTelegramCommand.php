<?php

namespace App\TelegramCommand;

use App\Entity\TelegramCommand;
use App\Interfaces\TelegramCommandInterface;
use App\Repository\TelegramCommandRepository;

class HelpTelegramCommand implements TelegramCommandInterface
{
    /**
     * @var TelegramCommandRepository
     */
    protected $repository;

    /**
     * HelpTelegramCommand constructor.
     *
     * @param TelegramCommandRepository $repository
     */
    public function __construct(TelegramCommandRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $commands = $this->repository
            ->findAll();

        $message = '';

        foreach ($commands as $command){
            /** @var TelegramCommand $command */
            $message .= sprintf("%s - %s \n", $command->getName(), $command->getDescription());
        }

        return $message;
    }

    public function setChatId(int $chatId): void
    {
    }

    public function setArgs(string $args):void
    {
    }
}
