<?php

namespace App\TelegramCommand;

use App\Interfaces\TelegramCommandInterface;

class StartTelegramCommand implements TelegramCommandInterface
{
    /**
     * @return string
     */
    public function render(): string
    {
        return 'Hey !';
    }

    public function setChatId(int $chatId): void
    {
    }

    public function setArgs(string $args):void
    {
    }
}
