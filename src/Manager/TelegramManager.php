<?php

namespace App\Manager;

use Psr\Log\LoggerInterface;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;
use unreal4u\TelegramAPI\Exceptions\ClientException;
use unreal4u\TelegramAPI\HttpClientRequestHandler;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\TgLog;

class TelegramManager
{
    /**
     * @var LoopInterface
     */
    protected $loop;

    /**
     * @var HttpClientRequestHandler
     */
    protected $handler;

    /**
     * @var TgLog
     */
    protected $telegramLog;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * TelegramManager constructor.
     *
     * @param LoggerInterface $logger
     * @param string          $botToken
     */
    public function __construct(LoggerInterface $logger, string $botToken)
    {
        $this->logger = $logger;
        $this->loop = Factory::create();
        $this->handler = new HttpClientRequestHandler($this->loop);
        $this->telegramLog = new TgLog($botToken, $this->handler);
    }

    /**
     * @param string $chatId
     * @param string $message
     *
     * @return TelegramManager
     */
    public function sendMessage( string $chatId, string $message ): self
    {
        $sendMessage = $this->message($chatId, $message);
        $this->send($sendMessage);

        $this->loop->run();

        return $this;
    }

    /**
     * @param string $chatId
     * @param string $message
     * @param array  $markup
     *
     * @return TelegramManager
     */
    public function sendMessageWithInlineKeyboard(string $chatId, string $message, array $markup):self
    {
        $sendMessage = $this->message($chatId, $message);

        $inlineKeyboard = new Markup([
            'inline_keyboard' => $markup
        ]);

        $sendMessage->disable_web_page_preview = true;
        $sendMessage->parse_mode = 'Markdown';
        $sendMessage->reply_markup = $inlineKeyboard;

        $this->send($sendMessage);

        $this->loop->run();

        return $this;
    }

    /**
     * @param string $chatId
     * @param string $message
     *
     * @return SendMessage
     */
    protected function message(string $chatId, string $message):SendMessage
    {
        $sendMessage = new SendMessage();
        $sendMessage->chat_id = $chatId;
        $sendMessage->parse_mode = 'html';
        $sendMessage->text = $message;

        $this->logger->notice($message);

        return $sendMessage;
    }

    /**
     * @param SendMessage $sendMessage
     *
     * @return TelegramManager
     */
    protected function send(SendMessage $sendMessage):self
    {
        $promise = $this->telegramLog->performApiRequest($sendMessage);

        $promise->then(
            function ($response) {
                $this->logger->notice('Message Send');
            },
            function (ClientException $exception) {
                $this->logger->critical($exception->getMessage());
            }
        );

        return $this;
    }
}
