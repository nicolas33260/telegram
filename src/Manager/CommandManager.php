<?php


namespace App\Manager;


use App\Entity\TelegramCommand;
use App\Interfaces\TelegramCommandInterface;
use App\Repository\TelegramCommandRepository;
use Psr\Container\ContainerInterface;

class CommandManager
{
    /**
     * @var TelegramCommandRepository
     */
    protected $repository;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * CommandManager constructor.
     *
     * @param TelegramCommandRepository $repository
     * @param ContainerInterface        $container
     */
    public function __construct(TelegramCommandRepository $repository, ContainerInterface $container)
    {
        $this->repository = $repository;
        $this->container = $container;
    }

    /**
     * @param string $command
     * @param int    $chatId
     *
     * @return string
     */
    public function process(string $command, int $chatId):string
    {
        /**
         * Command with arguments can be parsed with | character
         */
        $data = explode('|', $command);

        $command = $data[0];
        $args = isset($data[1]) ? $data[1] : null;

        /** @var TelegramCommand $telegramCommand */
        $telegramCommand = $this->repository
            ->findOneBy(['name' => $command, 'callback' => false]);

        if( !$telegramCommand ) {
            return 'Command not found';
        }

        $className = $telegramCommand->getClassName();

        /** @var TelegramCommandInterface $class */
        $class = $this->container->get($className);
        $class->setChatId($chatId);

        if(null !== $args){
            $class->setArgs($args);
        }

        return $class->render();
    }
}
