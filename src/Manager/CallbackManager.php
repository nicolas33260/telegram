<?php


namespace App\Manager;


use App\Entity\TelegramCommand;
use App\Interfaces\TelegramCommandInterface;
use App\Repository\TelegramCommandRepository;
use App\TelegramCommand\CheckTelegramCommand;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CallbackManager
{
    /**
     * @var TelegramCommandRepository
     */
    protected $repository;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * CommandManager constructor.
     *
     * @param TelegramCommandRepository $repository
     * @param ContainerInterface        $container
     */
    public function __construct(TelegramCommandRepository $repository, ContainerInterface $container)
    {
        $this->repository = $repository;
        $this->container = $container;
    }

    /**
     * @param string $command
     *
     * @return string
     */
    public function process(string $command):string
    {
        /**
         * Command with arguments can be parsed with | character
         */
        list($command, $args) = explode('|', $command);

        /** @var TelegramCommand $telegramCommand */
        $telegramCommand = $this->repository
            ->findOneBy(['name' => $command, 'callback' => true]);

        if( !$telegramCommand ) {
            return 'Command not found';
        }

        $className = $telegramCommand->getClassName();

        /** @var TelegramCommandInterface $class */
        $class = $this->container->get($className);
        $class->setArgs($args);

        return $class->render();
    }
}
