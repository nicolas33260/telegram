<?php


namespace App\Manager;


use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $manager;

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var TelegramManager
     */
    protected $telegramManager;

    /**
     * UserManager constructor.
     *
     * @param EntityManagerInterface $manager
     * @param UserRepository         $repository
     * @param TelegramManager        $telegramManager
     */
    public function __construct(EntityManagerInterface $manager, UserRepository $repository, TelegramManager $telegramManager)
    {
        $this->manager = $manager;
        $this->repository = $repository;
        $this->telegramManager = $telegramManager;
    }

    /**
     * @param int $chatId
     *
     * @return User|null
     */
    public function fetch(int $chatId):?User
    {
        return $this->repository
            ->findOneBy(['chatId' => $chatId]);
    }

    /**
     * @param string $name
     * @param int    $chatId
     *
     * @return User
     *
     * @throws \Exception
     */
    public function create(string $name, int $chatId):User
    {
        $user = (new User())
            ->setName($name)
            ->setCreatedAt(new \DateTime())
            ->setChatId($chatId)
            ->setEnabled(false);

        $this->manager->persist($user);
        $this->manager->flush();

        $this->telegramManager->sendMessage(
            $chatId,
            "Votre inscription a bien été prise en compte.\nUn administrateur doit valider votre demande."
        );

        $this->requestAdminDecision(
            $user->getName() . ' veut accèder au bot',
            $user
        );

        return $user;
    }

    /**
     * @param string $message
     * @param User   $user
     *
     * @return UserManager
     */
    public function requestAdminDecision(string $message, User $user):self
    {
        $admin = $this->repository->findOneBy(['admin' => true]);

        $markup = [
            [
                ['text' => 'Accepter', 'callback_data' => '/user|'.$user->getId().'=yes'],
                ['text' => 'Refuser', 'callback_data' => '/user|'.$user->getId().'=no']
            ]
        ];

        $this->telegramManager
            ->sendMessageWithInlineKeyboard(
                $admin->getChatId(),
                $message,
                $markup
            );

        return $this;
    }
}
