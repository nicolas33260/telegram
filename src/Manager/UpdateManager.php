<?php


namespace App\Manager;


class UpdateManager
{
    /**
     * @var TelegramManager
     */
    protected $telegramManager;

    /**
     * @var CommandManager
     */
    protected $commandManager;

    /**
     * @var CallbackManager
     */
    protected $callbackManager;

    /**
     * UpdateManager constructor.
     *
     * @param TelegramManager $telegramManager
     * @param CommandManager  $commandManager
     * @param CallbackManager $callbackManager
     */
    public function __construct(TelegramManager $telegramManager, CommandManager $commandManager, CallbackManager $callbackManager)
    {
        $this->telegramManager = $telegramManager;
        $this->commandManager = $commandManager;
        $this->callbackManager = $callbackManager;
    }

    /**
     * @param array $message
     */
    public function process( array $message ):void
    {
        $chatId = $message['from']['id'];
        $text = $message['text'];

        $this->telegramManager->sendMessage(
            $chatId,
            $this->commandManager->process($text, $chatId)
        );
    }

    /**
     * @param array $message
     */
    public function processCallback( array $message ):void
    {
        $chatId = $message['from']['id'];
        $text = $message['data'];

        $this->telegramManager->sendMessage(
            $chatId,
            $this->callbackManager->process($text)
        );
    }
}
