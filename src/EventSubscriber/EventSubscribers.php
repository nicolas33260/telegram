<?php

namespace App\EventSubscriber;

use App\Manager\UserManager;
use App\Service\UserService;
use Exception;
use App\Entity\User;
use App\Manager\TelegramManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class EventSubscribers
 *
 * @package App\EventSubscriber
 */
class EventSubscribers implements EventSubscriberInterface
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * EventSubscribers constructor.
     *
     * @param UserService     $userService
     * @param RouterInterface $router
     */
    public function __construct(UserService $userService, RouterInterface $router)
    {
        $this->router = $router;
        $this->userService = $userService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents():array
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest'],
        ];
    }

    /**
     * @param RequestEvent $event
     *
     * @throws Exception
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();

        if ( $request->getRequestUri() !== $this->router->generate('telegram_updates') ) {
            return;
        }

        $check = $this->userService->checkUser($request);

        if( $check instanceof Response){
            $event->setResponse($check);
        }
    }
}
