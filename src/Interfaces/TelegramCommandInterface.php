<?php

namespace App\Interfaces;

interface TelegramCommandInterface
{
    /**
     * Return the message for the command
     *
     * @return string
     */
    public function render():string;

    /**
     * @param string $args
     */
    public function setArgs(string $args):void;

    /**
     * @param int $chatId
     */
    public function setChatId(int $chatId):void;
}
