Telegram Webhook API
=====================

Api Telegram pour communiquer avec un bot en Symfony 4

###Installation

```bash
composer install
```

###Configuration

#####Envrionnement
```dotenv
###> .env <###

DATABASE_URL=mysql://USER:PASSWORD@HOST:3306/telegram
BOT_TOKEN=change-me
```

#####Base de données
```bash
php bin/console doctrine:database:create
php bin/console doctrine:schema:update -f
```

###Telegram

#####Webhook
    https://api.telegram.org/bot{BOT_TOKEN}/setWebhook?url={URL}

ok
